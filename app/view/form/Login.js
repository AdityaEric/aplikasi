Ext.define('Kelompok.view.form.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        
        'Ext.field.Password'
        
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype:'login',
    controller:'login',
    id: 'login',
    items: [{
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: '<center>LOGIN FORM</center>',
            instructions: 'Lupa Kata Sandi?',
            defaults: {
                labelWidth: '35%'
            },




            items: [
                {
                    xtype: 'textfield',
                    name: 'UserName',
                    label: 'User Name',

                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
                
            ]
        },
        
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    //scope: this,
                    hasDisabled: false,
                    handler:'onLogin'
                }
                
                
            ]
        }
    ]
});