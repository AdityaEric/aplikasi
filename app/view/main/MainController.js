/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Kelompok.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        var name_brg = record.data.name_brg;
        var harga = record.data.harga;
        localStorage.setItem('name_brg', name_brg);
        localStorage.setItem('harga', harga);
        Ext.Msg.confirm('Confirm', 'Apakah Anda Ingin '+name_brg+'?', 'onConfirm', this);
        console.log(record.data);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            alert('Item dipilih');
        }
        else {
            alert('Item belum dipilih');
        }
    }
});
