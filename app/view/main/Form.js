Ext.define('Kelompok.view.main.Form', {
    extend: 'Ext.form.Panel',
    xtype: 'myprofil',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'basicform',
    items: [{
        xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items: [{
                xtype: 'component'
            },
            {
                flex: 4,
                iconCls: 'x-fa fa-bars',
                text: 'BOKING',
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                }
            }]
        },
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: '<center>PROFIL</center>',
            //instructions: 'Mohon Di Isi Seluruhnya.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Name',
                    placeHolder: 'Aditya Eric Wahyudi',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
                {
                    xtype: 'selectfield',
                    name: 'jk',
                    label: 'Jenis Kelamin',
                    options: [
                        {
                            text: 'Laki-Laki',
                            value: 'Laki-Laki'
                        },
                        {
                            text: 'Perempuan',
                            value: 'Perempuan'
                        }
                    ]
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'Adityaericw@gmail.com',
                    clearIcon: true
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: 'Tanggal Lahir',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },
                
                {
                    xtype: 'textareafield',
                    name: 'alamat',
                    label: 'Alamat'
                }
            ]
        },
        
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'SIMPAN',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        var fieldset1 = Ext.getCmp('fieldset1'),
                            fieldset2 = Ext.getCmp('fieldset2');

                        if (btn.hasDisabled) {
                            fieldset1.enable();
                            fieldset2.enable();
                            btn.hasDisabled = false;
                            btn.setText('Disable fields');
                        } else {
                            fieldset1.disable();
                            fieldset2.disable();
                            btn.hasDisabled = true;
                            btn.setText('Enable fields');
                        }
                    }
                },
                {
                    text: 'RESET',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                }
            ]
        }
    ]
});