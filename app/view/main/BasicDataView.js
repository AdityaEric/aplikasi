Ext.define('Kelompok.view.main.BasicDataView', {
    extend: 'Ext.Container',
    xtype : 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Kelompok.store.Personnel'
    ],

    viewModel: {
        stores : {
            personnel: {
                type : 'personnel'
            }
        }
    },
    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
        xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items: [{
                xtype: 'component'
            },
            {
                flex: 4,
                iconCls: 'x-fa fa-bars',
                text: 'BOOKING',
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                }
            }]
        },  {
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="img"><img src="resources/gambarbarang/{foto}" width=150></div><div class="content"><div class="name"><font size = "3">{name_brg}</font></div><div class="affiliation"><b>{booking}</b></div><div class="affiliation"><b>{harga}</b></div></div>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                '<tr><td>BOOKING    : </td><td>{name_brg}</td></tr>' +
                '<tr><td>TUJUAN    : </td><td>{booking}</td></tr>' +
                '<tr><td>HARGA   :</td><td>{harga}</td></tr>' 
        },
        listeners: {
            select: 'onItemSelected'
        }

    }]
});