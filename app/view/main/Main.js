/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Kelompok.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Kelompok.view.main.MainController',
        'Kelompok.view.main.MainModel',
        'Kelompok.view.main.Carousel',
        'Kelompok.view.main.BasicDataView',
        'Kelompok.view.main.Form',
        'Kelompok.view.form.Login'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Booking',
            iconCls: 'x-fa fa-plane',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Beranda',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview',
            }]
        },{
            title: 'Profil',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'myprofil',
            }]
        }
    ]
});
