/**
 * This view is an example list of people.
 */
Ext.define('Kelompok.view.main.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',
    requires: [
        'Ext.carousel.Carousel',
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Kelompok.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    styleHtmlContent: true,
    items: [{
        //docked: 'top',
        //xtype: 'toolbar',
        //items: [
        //{
        //xtype: 'searchfield',
        //placeHolder: 'Search',
        //name: 'searchfield'
        //}
        //]
        //},
            xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items: [{
                xtype: 'component'
            },
            {
                flex: 4,
                iconCls: 'x-fa fa-bars',
                text: 'BOOKING',
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                }
            }]
        },  {
        xtype: 'carousel',
        direction:'vertical',
        items: [{
            html: '<img src="resources/gambarcarousel/2.jpg" width=350 height=310>'

        },
        {
            html: '<img src="resources/gambarcarousel/3.png" width=350 height=310>'
        },
        {
            html: '<img src="resources/gambarcarousel/4.jpg" width=350 height=310>'
        }]
    }, {
        layout: 'fit',

        items: [{
            
            xtype: 'dataview',
            scrollable: 'y',
            cls: 'dataview-basic',
            itemTpl: '<div class="img"><img src="resources/gambarbarang/{foto}" width=300></div><div class="content"><div class="name"><font size="3"></b>{name_brg}</b></font></div><div class="affiliation"><b>{booking}</b></div><div class="affiliation"><b>{harga}</b></div></div>',
            bind: {
                store: '{personnel}'
            },

            listeners: {
                select: 'onItemSelected'
            }

        }]
    }],
    initialize: function() {
        Ext.Viewport.setMenu(this.getMenuCfg('left'), {
            side: 'left'
        });

    },

    doDestroy: function() {
        Ext.Viewport.removeMenu('left');
        this.callParent();
    },

    getMenuCfg: function(side) {
        return {
            items: [{
                text: 'Profil',
                iconCls: 'x-fa fa-user',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                text: 'Cari',
                iconCls: 'x-fa fa-search',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                xtype: 'button',
                text: 'Setting',
                iconCls: 'x-fa fa-gear',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                xtype: 'button',
                text: 'Pembayaran',
                iconCls: 'x-fa fa-plane',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }]
        };
    }
});