Ext.define('Kelompok.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    fields: [
        'foto', 'name_brg', 'harga','booking' 
    ],

    data: { items: [
        { foto: 'pesawat.png' , name_brg: 'Tiket Pesawat-CITILINK' , booking: 'Jakarta-Pekanbaru', harga: "Rp. 1.279.600"},
        { foto: 'pesawat.png' , name_brg: 'Tiket Pesawat-LION' , booking: 'Medan-Jakarta', harga: "Rp. 1.092.500"},
        { foto: 'pesawat.png' , name_brg: 'Tiket Pesawat-LION', booking: 'Pekanbaru-Yogyakarta' , harga: "Rp. 1.695.400"},
        { foto: 'hotel.jpg' , name_brg: 'Booking Hotel', booking: 'Jakarta' , harga: "355.476/Malam"},
        { foto: 'hotel.jpg' , name_brg: 'Booking Hotel', booking: 'Jakarta' , harga: "278.519/Malam"},
        { foto: 'kapal.png' , name_brg: 'Tiket Kapal(BINAIYA)', booking: 'Tanjung-Perak' , harga: "Rp. 200.000"},
        { foto: 'kapal.png' , name_brg: 'Tiket Kapal(BINAIYA)', booking: 'Labuan Bajo-Makassar' , harga: "Rp. 161.000"},
        { foto: 'kereta.jpg' , name_brg: 'Tiket Kereta Api', booking: 'Bandung-Surabaya' , harga: "Rp. 370.000"},
        { foto: 'kereta.jpg' , name_brg: 'Tiket Kereta Api ', booking: 'Jakarta-Semarang' , harga: "Rp. 265.000"}
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
